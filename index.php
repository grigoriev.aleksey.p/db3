<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Спасибо, результаты сохранены.');
  }
  include('form.php');
  exit();
}

$errors = FALSE;

if (empty($_POST['fio'])) {
    print('Заполните имя.<br>');
    $errors = TRUE;
}
else if (!preg_match("/^[а-яА-Я ]+$/u", $_POST['fio'])) {
    print('Недопустимые символы в имени.<br>');
    $errors = TRUE;
}

//email
if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    print('Проверьте правильность ввода email<br>');
    $errors = TRUE;
}

//year
if (empty($_POST['bd'])) {
    print('Заполните год.<br>');
    $errors = TRUE;
}
else {
    $year = $_POST['bd'];
    if (!(is_numeric($year) && intval($year) >= 1900 && intval($year) < 2020)) {
        print("Укажите корректный год.<br>");
        $errors = TRUE;
    }
}



//abilities
$ability_data = ['immort', 'wall', 'levit', 'invis'];
if (empty($_POST['abilities'])) {
    print('Выберите способность<br>');
    $errors = TRUE;
}
else {
    $abilities = $_POST['abilities'];
    foreach ($abilities as $ability) {
        if (!in_array($ability, $ability_data)) {
            print('Недопустимая способность<br>');
            $errors = TRUE;
        }
    }
}
$ability_insert = [];
foreach ($ability_data as $ability) {
    $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
}


if ($errors) {
  exit();
}

$user = 'u40078';
$pass = '8932258';
$db = new PDO('mysql:host=localhost;dbname=u40078', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $stmt = $db->prepare("INSERT INTO forms SET fio = ?, email = ?, bd = ?, sex = ?, limbs = ?, bio = ?");
  $stmt -> execute([$_POST['fio'], $_POST['email'], $_POST['bd'], $_POST['sex'], intval($_POST['limbs']), $_POST['bio']]);
  $stmt2 = $db->prepare("INSERT INTO user_ab SET id = ?, immort = ?,  wall = ?, levit = ?, invis = ?");
  $id = $db->lastInsertId();
  $stmt2 -> execute([$id, $ability_insert['immort'], $ability_insert['wall'], $ability_insert['levit'], $ability_insert['invis']]);
}
catch(PDOException $e) {
    print('Error : ' . $e->getMessage());
    exit();
}

header('Location: ?save=1');
